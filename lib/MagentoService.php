<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-06-13
 * Time: 21:50
 */

namespace Kowal\ImportStockAndPriceFromCsv\lib;


class MagentoService
{
    public $store_id = 0;
    public $max_stock_as_zero = 0;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection
    )
    {
        $this->resourceConnection = $resourceConnection;
    }


    /**
     * @return mixed
     */
    public function _getReadConnection()
    {
        return $this->_getConnection('core_read');
    }

    /**
     * @return mixed
     */
    public function _getWriteConnection()
    {
        return $this->_getConnection('core_write');
    }

    /**
     * @param string $type
     * @return mixed
     */
    private function _getConnection($type = 'core_read')
    {
        return $this->resourceConnection->getConnection($type);
    }

    /**
     * @param $tableName
     * @return mixed
     */
    private function _getTableName($tableName)
    {
        return $this->resourceConnection->getTableName($tableName);
    }

    /**
     * @param $attributeCode
     * @return mixed
     */
    private function _getAttributeId($attributeCode)
    {
        $connection = $this->_getReadConnection();
        $sql = "SELECT attribute_id FROM " . $this->_getTableName('eav_attribute') . " WHERE entity_type_id = ? AND attribute_code = ?";
        return $connection->fetchOne(
            $sql,
            [
                $this->_getEntityTypeId('catalog_product'),
                $attributeCode
            ]
        );
    }

    /**
     * @param $entityTypeCode
     * @return mixed
     */
    private function _getEntityTypeId($entityTypeCode)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_type_id FROM " . $this->_getTableName('eav_entity_type') . " WHERE entity_type_code = ?";
        return $connection->fetchOne(
            $sql,
            [
                $entityTypeCode
            ]
        );
    }

    /**
     * @param $sku
     * @return mixed
     */
    private function _getIdFromSku($sku)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_id FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku = ?";
        return $connection->fetchOne(
            $sql,
            [
                $sku
            ]
        );
    }

    /**
     * @param $sku
     * @return mixed
     */
    public function checkIfSkuExists($sku)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT COUNT(*) AS count_no FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku = ?";
        return $connection->fetchOne($sql, [$sku]);
    }

    /**
     * @param $sku
     * @param $price
     * @param int $storeId
     */
    public function updatePrices($sku, $price)
    {
        $connection = $this->_getWriteConnection();
        $entityId = $this->_getIdFromSku($sku);
        $attributeId = $this->_getAttributeId('price');
        $sql = "INSERT INTO " . $this->_getTableName('catalog_product_entity_decimal') . " (attribute_id, store_id, entity_id, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)";
        $connection->query(
            $sql,
            [
                $attributeId,
                $this->store_id,
                $entityId,
                $price
            ]
        );
    }

    /**
     * @param $sku
     * @param $newQty
     */
    public function _updateStocks($sku, $newQty)
    {
        $connection = $this->_getWriteConnection();
        $productId = $this->_getIdFromSku($sku);

//        $sql = "UPDATE " . $this->_getTableName('cataloginventory_stock_item') . " csi,
//					   " . $this->_getTableName('cataloginventory_stock_status') . " css
//	                   SET
//					   csi.qty = ?,
//					   csi.is_in_stock = ?,
//	                   css.qty = ?,
//					   css.stock_status = ?
//					   WHERE
//					   csi.product_id = ?
//			           AND csi.product_id = css.product_id";
//        $connection->query($sql, array($newQty, $isInStock, $newQty, $stockStatus, $productId));

        $sql = "UPDATE " . $this->_getTableName('cataloginventory_stock_item') . " csi
	                   SET
					   csi.qty = ?,
					   csi.manage_stock = 1,
					   csi.use_config_manage_stock = 0,
					   csi.is_in_stock = ?
					   WHERE
					   csi.product_id = ?";

        $sql2 = "UPDATE 
					   " . $this->_getTableName('cataloginventory_stock_status') . " css
	                   SET
	                   css.qty = ?,
					   css.stock_status = ?
					   WHERE
					   css.product_id = ?";

        $isInStock = $newQty > $this->max_stock_as_zero ? 1 : 0;  // jeśli newQty większe od 2. Indywidualne ustawienie dla lampywcentrum
        $stockStatus = $newQty > $this->max_stock_as_zero ? 1 : 0; // jeśli newQty większe od 2. Indywidualne ustawienie dla lampywcentrum
        $visibility = $newQty > $this->max_stock_as_zero ? 4 : 3; // jeśli newQty większe od 2. Indywidualne ustawienie dla lampywcentrum

        $connection->query($sql, array($newQty, $isInStock, $productId));
        $connection->query($sql2, array($newQty, $stockStatus, $productId));

        $this->setProduktChwilowoNieDostepny($productId, $stockStatus, $connection);
        $this->setProduktVisibility($productId, $visibility, $connection);
        $this->setNextAvailabilityDate($productId, $visibility, $connection);
    }

    public function _updateNextAvalibityDate($sku, $date)
    {
        $connection = $this->_getWriteConnection();
        $productId = $this->_getIdFromSku($sku);
        $this->setNextAvailabilityDate($productId, $date, $connection);
    }

    public function setProduktChwilowoNieDostepny($entityId, $stockStatus, $connection)
    {
        $attributeId = $this->_getAttributeId('produkt_chwilowo_niedostepny');
        $sql = "INSERT INTO " . $this->_getTableName('catalog_product_entity_int') . " (attribute_id, store_id, entity_id, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)";
        $connection->query(
            $sql,
            [
                $attributeId,
                $this->store_id,
                $entityId,
                !$stockStatus
            ]
        );
    }

    public function setNextAvailabilityDate($entityId,$NextAvailabilityDate,$connection){
        $attributeId = $this->_getAttributeId('next_availability_date');
        $sql = "INSERT INTO " . $this->_getTableName('catalog_product_entity_datetime') . " (attribute_id, store_id, entity_id, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)";
        $connection->query(
            $sql,
            [
                $attributeId,
                $this->store_id,
                $entityId,
                $NextAvailabilityDate
            ]
        );
    }

    /**
     * @param $entityId
     * @param $visibility [1=>not visable, 2,=>catalog, 3=>search, 4=>catalog search]
     * @param $connection
     */
    public function setProduktVisibility($entityId, $visibility, $connection)
    {
        $niedostepne_widoczne = (int)$this->getAttributeValue($entityId, $this->_getAttributeId('niedostepne_widoczne'), 'niedostepne_widoczne');
        if ($niedostepne_widoczne == 1 AND $visibility == 3) $visibility = 4;  // wyjątek

        $attributeId = $this->_getAttributeId('visibility');
        $sql = "INSERT INTO " . $this->_getTableName('catalog_product_entity_int') . " (attribute_id, store_id, entity_id, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)";
        $connection->query(
            $sql,
            [
                $attributeId,
                $this->store_id,
                $entityId,
                $visibility
            ]
        );
    }

    function getAttributeValue($productId, $attributeId, $attributeKey)
    {
        try {
            $attribute_type = $this->_getAttributeType($attributeKey);

            if ($attribute_type == 'static') return false;

            $connection = $this->_getConnection('core_write');

            $sql = "SELECT value FROM " . $this->_getTableName('catalog_product_entity_' . $attribute_type) . " cped
			WHERE  cped.attribute_id = ?
			AND cped.entity_id = ?";
            return $connection->fetchOne($sql, array($attributeId, $productId));

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function _getAttributeType($attribute_code = 'price')
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT backend_type
				FROM " . $this->_getTableName('eav_attribute') . "
			WHERE
				entity_type_id = ?
				AND attribute_code = ?";
        $entity_type_id = $this->_getEntityTypeId('catalog_product');
        return $connection->fetchOne($sql, array($entity_type_id, $attribute_code));
    }
}