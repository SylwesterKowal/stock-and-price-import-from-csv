<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-06-13
 * Time: 22:57
 */

namespace Kowal\ImportStockAndPriceFromCsv\lib;


class SourceService
{

    public $csvFile;
    public $local_file_path = ""; // np: importexport

    public $headers = [];
    public $prefixSkuColumnName = '';
    public $skuColumnName = '';
    public $priceColumnName = '';
    public $stockColumnName = '';
    public $dateColumnName = '';
    public $dateColumnFormat = '';
    public $dateColumnFormatSeparator = '';
    public $host = "";
    public $ftp_user_name = "";
    public $ftp_user_pass = "";
    public $sourceByFtp = false;
    public $sourceFolderPathOrUrl = "";

    /**
     * @param $field
     * @return false|int|string
     */
    public function getIndex($field)
    {
        try {
            $index = array_search($field, $this->headers);
            if (!strlen($index)) {
                $index = -1;
            }
            return $index;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @return array
     */
    public function readCsvRows($delimter = ",")
    {

        $rows = [];
        $fileHandle = fopen($this->local_file_path . $this->csvFile, 'r');
        while (($row = fgetcsv($fileHandle, 0, $delimter, '"', '"')) !== false) {
            $rows[] = $row;
        }
        fclose($fileHandle);

        $this->headers = array_shift($rows);
        echo print_r($this->headers, true);
        return $rows;
    }

    public function getFilesFromFtp($source_file_name)
    {
        if ($this->sourceByFtp) {

            $server_file = $source_file_name;

            $conn_id = ftp_connect($this->host);

            $login_result = ftp_login($conn_id, $this->ftp_user_name, $this->ftp_user_pass);

            echo "source file: " . $source_file_name . "\n";
            echo "local file: " . $this->local_file_path . $source_file_name . "\n";


            if (ftp_get($conn_id, $this->local_file_path . $source_file_name, $source_file_name, FTP_BINARY)) {
                echo "Successfully written to {$this->local_file_path}{$source_file_name}\n";
            } else {
                echo "There was a problem\n";
            }

            ftp_close($conn_id);
        } else {
            file_put_contents($this->local_file_path . $source_file_name,file_get_contents( $this->sourceFolderPathOrUrl));
        }
    }


    /**
     * Odczyt danych z pliku CSV
     * @param $csvData
     */
    public function prepareCsvFile($csvData)
    {

        $fp = fopen($this->local_file_path . $this->csvFile, 'w');

        fputcsv($fp, ['sku', 'price', 'stock', 'date']);
        foreach ($csvData as $_data) {

            try {
                if (isset($_data[$this->getIndex($this->skuColumnName)])) {
                    $sku = $this->prefixSkuColumnName . $_data[$this->getIndex($this->skuColumnName)];
                    $price = (isset($_data[$this->getIndex($this->priceColumnName)])) ? (float)str_replace(",", ".", $_data[$this->getIndex($this->priceColumnName)]) : null;
                    $stock = $this->getStock($_data); // (isset($_data[$this->getIndex($this->stockColumnName)])) ? (float)str_replace(",", ".", $_data[$this->getIndex($this->stockColumnName)]) : null;
                    $date = (isset($_data[$this->getIndex($this->dateColumnName)])) ? $this->parseDateFormat($_data[$this->getIndex($this->dateColumnName)]) : null;

                    fputcsv($fp, [$sku, $price, $stock, $date]);
                }
            } catch (Exception $e) {
                echo $e->getTraceAsString() . "\n";
            }
        }

        fclose($fp);
    }

    /**
     * Prztwozenie kolumny lub kolumn z stanem magazynowym
     * @param $_data
     * @return null
     */
    private function getStock($_data)
    {
        $stock = null;
        if (strpos($this->stockColumnName, '|') !== false) {
            $cols = explode("|", $this->stockColumnName);
            print_r($cols);
            $stock = 0;
            foreach ($cols as $key => $col) {
                $stock_ = (isset($_data[$this->getIndex($col)])) ? (float)str_replace(",", ".", $_data[$this->getIndex($col)]) : 0;
                $stock = $stock + $stock_;
            }
        } else {
            $stock = (isset($_data[$this->getIndex($this->stockColumnName)])) ? (float)str_replace(",", ".", $_data[$this->getIndex($this->stockColumnName)]) : null;
        }

        return $stock;
    }

    private function parseDateFormat($date)
    {
        if (!empty($date)) {
            switch ($this->dateColumnFormat) {
                case "99 XXXX ww/yyyy":
                    $dateArray = explode($this->dateColumnFormatSeparator, $date);
                    if (strpos(trim($dateArray[0]), '/') !== false) {

                        $week = explode("/", substr(trim($dateArray[0]), -7));
                        if(isset($week[0]) && isset($week[1]) && $week[0] > 0 && strlen($week[1]) == 4) {
                            $week_array = $this->getStartAndEndDate($week[0], $week[1]);
                            return $week_array['week_end'];
                        }else{
                            return null;
                        }
                    }else{
                        return null;
                    }
                    break;
                default:
                    $dateArray = explode($this->dateColumnFormatSeparator, $date);
                    $dateFormatArray = explode($this->dateColumnFormatSeparator, $this->dateColumnFormat);

                    $day = $dateArray[array_search("dd", $dateFormatArray)];
                    $month = $dateArray[array_search("mm", $dateFormatArray)];
                    $year = $dateArray[array_search("yyyy", $dateFormatArray)];
                    if (!empty($day) && !empty($month) && !empty($year)) {
                        $data = $year . "-" . $month . "-" . $day;
                        return $data;
                    } else {
                        return null;
                    }
            }


        } else {
            return null;
        }
    }

    private function getStartAndEndDate($week, $year)
    {
        $dto = new \DateTime();
        $dto->setISODate($year, $week);
        $ret['week_start'] = $dto->format('Y-m-d');
        $dto->modify('+6 days');
        $ret['week_end'] = $dto->format('Y-m-d');
        return $ret;
    }


}