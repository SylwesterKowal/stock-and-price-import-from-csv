<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-06-13
 * Time: 23:55
 */

namespace Kowal\ImportStockAndPriceFromCsv\lib;


class Worker
{

    /**
     * @var \Kowal\ImportStockAndPriceFromCsv\lib\MagentoService
     */
    protected $magentoService;

    /**
     * @var Kowal\ImportStockAndPriceFromCsv\lib\SourceService
     */
    protected $sourceService;

    /**
     * @var ScopeConfig
     */
    protected $scopeConfig;

    /**
     * Worker constructor.
     * @param MagentoService $magentoService
     * @param SourceService $sourceService
     * @param ScopeConfig $scopeConfig
     */
    public function __construct(
        \Kowal\ImportStockAndPriceFromCsv\lib\MagentoService $magentoService,
        \Kowal\ImportStockAndPriceFromCsv\lib\SourceService $sourceService,
        \Kowal\ImportStockAndPriceFromCsv\lib\ScopeConfig $scopeConfig,
        \Magento\Framework\Filesystem\DirectoryList $dir
    )
    {
        $this->magentoService = $magentoService;
        $this->sourceService = $sourceService;
        $this->scopeConfig = $scopeConfig;
        $this->_dir = $dir;
    }


    public function execute($production = 1, $sourcename = '')
    {
        try {

            $config = $this->scopeConfig->getScopeConfig($sourcename);
            echo print_r($config,true);
            if (!$config['aktywny']) {
                echo "Import " . $sourcename . " jest nieaktywny". "\r";
                return;
            }



            $this->sourceService->sourceByFtp = $config['source_by_ftp'];
            $this->sourceService->sourceFolderPathOrUrl = $config['source_folder_path_or_url'];
            $this->sourceService->prefixSkuColumnName = $config['sku_prefix'];
            $this->sourceService->skuColumnName = $config['sku_name'];
            $this->sourceService->priceColumnName = ($config['price_name']) ? $config['price_name'] : false;
            $this->sourceService->stockColumnName = $config['stock_name'];
            $this->sourceService->dateColumnName = $config['date_name'];
            $this->sourceService->dateColumnFormat = $config['date_format'];
            $this->sourceService->dateColumnFormatSeparator = $config['date_separator'];
            $this->sourceService->local_file_path = $this->_dir->getPath('var') . DIRECTORY_SEPARATOR . $config['local_folder_name'] . DIRECTORY_SEPARATOR;

            $this->sourceService->host = $config['host'];
            $this->sourceService->ftp_user_name = $config['username'];
            $this->sourceService->ftp_user_pass = $config['password'];

            // odczyt z pliku CSV
            $this->sourceService->getFilesFromFtp($config['source_file_name']);
            $this->sourceService->csvFile = $config['source_file_name'];
            $csvData = $this->sourceService->readCsvRows($config['delimeter']);

            // przygotowanie pliku CSV sku,price,stock
            $this->sourceService->csvFile = "_" . $config['source_file_name'];
            $this->sourceService->prepareCsvFile($csvData);
            $csvData = $this->sourceService->readCsvRows(",");

            $count = 0;
            foreach ($csvData as $_data) {
                $sku = $_data[$this->sourceService->getIndex('sku')];
                $price = $_data[$this->sourceService->getIndex('price')];
                $stock = $_data[$this->sourceService->getIndex('stock')];
                $date = (empty($_data[$this->sourceService->getIndex('date')])) ? null : $_data[$this->sourceService->getIndex('date')];

                $this->magentoService->max_stock_as_zero = $config['stany_zerowe_od'];
                if (!$this->magentoService->checkIfSkuExists($sku)) {
                    echo $count . '. FAILURE:: Product with SKU (' . $sku . ') doesn\'t exist.' . "\n";
                    continue;
                }
                try {
                    if($config['update_price']) $this->magentoService->updatePrices($sku, $price);
                    if($config['update_stock'])  $this->magentoService->_updateStocks($sku, $stock);
                    if($config['update_date'])  $this->magentoService->_updateNextAvalibityDate($sku, $date);
                    echo $count . '. SUCCESS:: Updated SKU (' . $sku . ') with price (' . $price . ') and stock (' . $stock . ')' . ') and date (' . $date . ')'. "\n";
                } catch (Exception $e) {
                    echo $count . '. ERROR:: While updating  SKU (' . $sku . ') with Price (' . $price . ') => ' . $e->getMessage() . "\n";
                }
                if (!$production && $count > 2) break;
                $count++;
            }
        } catch (Exception $e) {
            echo $e->getTraceAsString() . "\r";
        }
    }
}