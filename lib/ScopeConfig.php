<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 13/07/2020
 * Time: 16:12
 */

namespace Kowal\ImportStockAndPriceFromCsv\lib;


class ScopeConfig
{

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->_scopeConfig = $scopeConfig;
    }

    public function getScopeConfig($group_id)
    {
        return [
            'aktywny' => $this->_scopeConfig->getValue($group_id . '/ustawienia/aktywny', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'source_by_ftp' => $this->_scopeConfig->getValue($group_id . '/ustawienia/source_by_ftp', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'username' => $this->_scopeConfig->getValue($group_id . '/ustawienia/username', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'password' => $this->_scopeConfig->getValue($group_id . '/ustawienia/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'host' => $this->_scopeConfig->getValue($group_id . '/ustawienia/host', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'port' => $this->_scopeConfig->getValue($group_id . '/ustawienia/port', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'local_folder_name' => $this->_scopeConfig->getValue($group_id . '/ustawienia/local_folder_name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'source_file_name' => $this->_scopeConfig->getValue($group_id . '/ustawienia/source_file_name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'source_folder_path_or_url' => $this->_scopeConfig->getValue($group_id . '/ustawienia/source_folder_path_or_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),

            'headers' => $this->_scopeConfig->getValue($group_id . '/konfiguracja_pliku/headers', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'delimeter' => $this->_scopeConfig->getValue($group_id . '/konfiguracja_pliku/delimeter', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'sku_prefix' => $this->_scopeConfig->getValue($group_id . '/konfiguracja_pliku/sku_prefix', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'sku_name' => $this->_scopeConfig->getValue($group_id . '/konfiguracja_pliku/sku_name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'price_name' => $this->_scopeConfig->getValue($group_id . '/konfiguracja_pliku/price_name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'stock_name' => $this->_scopeConfig->getValue($group_id . '/konfiguracja_pliku/stock_name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'date_name' => $this->_scopeConfig->getValue($group_id . '/konfiguracja_pliku/date_name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'date_format' => $this->_scopeConfig->getValue($group_id . '/konfiguracja_pliku/date_format', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'date_separator' => $this->_scopeConfig->getValue($group_id . '/konfiguracja_pliku/date_separator', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),

            'update_price' => $this->_scopeConfig->getValue($group_id . '/konfiguracja_zapisu/update_price', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'update_stock' => $this->_scopeConfig->getValue($group_id . '/konfiguracja_zapisu/update_stock', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'update_date' => $this->_scopeConfig->getValue($group_id . '/konfiguracja_zapisu/update_date', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'stany_zerowe_od' => $this->_scopeConfig->getValue($group_id . '/konfiguracja_zapisu/stany_zerowe_od', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),

        ];
    }


}