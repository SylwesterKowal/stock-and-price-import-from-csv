<?php


namespace Kowal\ImportStockAndPriceFromCsv\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportFrmCsv extends Command
{

    const STORE_ID_ARGUMENT = "stoe_id";
    const PROD_MODE = "produkcja";
    const SOURCE_NAME = "source_name";


    public function __construct(
        \Kowal\ImportStockAndPriceFromCsv\lib\Worker $worker
    )
    {
        parent::__construct();
        $this->worker = $worker;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        if (!$store_id = $input->getArgument(self::STORE_ID_ARGUMENT)) {
            $store_id = 0;
        }
        $test = $input->getOption(self::PROD_MODE);
        $sourcename = $input->getArgument(self::SOURCE_NAME);

        $output->writeln("Store " . $store_id);
        $output->writeln("Prod Mode " . (($test) ? "1" : "0"));
        $output->writeln("Source: " . $sourcename);

        $this->worker->execute($test, $sourcename);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_import:importfromcsv");
        $this->setDescription("Import z CSV");
        $this->setDefinition([
            new InputArgument(self::SOURCE_NAME, InputArgument::OPTIONAL, "Nazwa firmy z ktorej importujemy np: markslojd"),
            new InputArgument(self::STORE_ID_ARGUMENT, InputArgument::OPTIONAL, "Numer store_id"),
            new InputOption(self::PROD_MODE, "-p", InputOption::VALUE_NONE, "Domyślny tryb testowy. Podaj parametr -p by uruchomić tryb prod.")
        ]);
        parent::configure();
    }
}
